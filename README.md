# My Dev Steps and Documents

## Steps

1. Install and deploy
2. Set up layout, style, + Brift

## Docs

### MarkDown

- [Set Layout Markdown](https://docs.astro.build/en/core-concepts/layouts/#markdownmdx-layouts)

- [Web template - folder Structure](https://github.com/withastro/astro.build/blob/main/src/data/site-info.ts)

---

- Projects
- Note taking
- Contribute to my Git hub

- Reading:
  - technical books
  - self-dev books
