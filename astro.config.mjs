import { defineConfig } from "astro/config";
import mdx from "@astrojs/mdx";
import tailwind from "@astrojs/tailwind";

// https://astro.build/config
export default defineConfig({
  // sitemap: true,
  // site: "https://astro.build/",
  outDir: "public",
  publicDir: "static",
  integrations: [mdx({ drafts: true, optimize: true }), tailwind()],
});
